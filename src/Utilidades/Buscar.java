package Utilidades;

import Datos.Vendedor;
import Vehiculos.Vehiculo;
import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

public class Buscar {
//buscar_vehiculo busca el vehiculo en la lista de ventas  acorde a los parametros o caracteristicas que el usuario ingrese para ver la disponibilidad del mismo

    public static ArrayList<Venta> buscar_vehiculo(ArrayList<Venta> ventas, String tipo_vehiculo, double recorrido_min,
            double recorrido_max, int año_min, int año_max, double precio_min, double precio_max) {
        ArrayList<Venta> lista = new ArrayList<>(); // inicializa una nueva lista con objetos venntas
        for (Venta v : ventas) {// uso del for each para recorrer la lista ingresada como parametro
            Vehiculo vehiculo = v.getVehiculo();// almacena el vehiculo obtenido de la lista de ventas en otra variable
            if (vehiculo.getTipo().equals(tipo_vehiculo) && (vehiculo.getAño() >= año_min || vehiculo.getAño() <= año_max || vehiculo.getPrecio() >= precio_min || vehiculo.getPrecio() <= precio_max
                    || vehiculo.getRecorrido() >= recorrido_min || vehiculo.getRecorrido() <= recorrido_max)) { //si esta acorde a los requisitos del comprador se almacena en la nueva lista
                lista.add(v);
            }

        }
        return lista;

    }

    public static boolean verificarVehiculo(Vehiculo vehiculo) { //verifica con la placa si el vehiculo se encuentra
        int contador = 0;
        try (Scanner entrada = new Scanner(new File("vehiculos.txt"))) {
            while (entrada.hasNextLine()) {
                String[] linea = entrada.next().split(",");
                if (linea[1].equals(vehiculo.getPlaca())) { //compara si la linea[1] tiene la misma placa que la ingresada
                    contador++;
                }
            }
        } catch (Exception e) {

        }
        return contador > 0;//retorna true or false, true si se encuentra la placa del vehiculo (el contador debe ser mayor a cero)  o false en caso de no estar
    }

    public static boolean comprobar_usuario(String email, String user) {
        int contador = 0;
        try (Scanner entrada = new Scanner(new File("usuarios.txt"))) {
            while (entrada.hasNextLine()) {
                String[] linea = entrada.next().split(",");
                if (linea[3].equals(email) || linea[5].equals(user)) {
                    contador++;
                }
            }
        } catch (Exception e) {
        }
        return contador > 0;//retorna true de haber una coincidencia
    }

    public static boolean comprobar_usuario_BD(String contraseña, String cedula) {
        int contador = 0;
        try (Scanner entrada = new Scanner(new File("usuarios.txt"))) {
            while (entrada.hasNextLine()) {
                String[] linea = entrada.next().split(",");
                if (linea[6].equals(contraseña) && linea[0].equals(cedula)) {
                    contador++;
                }
            }
        } catch (Exception e) {
        }
        return contador > 0;
    }
//en la lista ofertas del vendedor se crea otra lista "ofertas", se busca en

    public static ArrayList<Oferta> Ofertas_de_vendedor(Vendedor vendedor, Vehiculo vehiculo) {
        ArrayList<Oferta> ofertas = new ArrayList<>();
        for (Venta v : vendedor.getListaVenta()) { //explora la lista venta que se encuentra en la clase vendedor
            if (v.getVehiculo().equals(vehiculo)) { //compara que los vehiculos sean iguales
                for (Oferta o : v.getOfertas()) {//explora la lista de oferta

                    ofertas.add(o);//agrega el objeto "o" a la lista ofertas
                }

            }

        }
        return ofertas;//retorna la lista
    }
}
