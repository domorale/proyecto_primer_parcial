package Utilidades;

import java.io.File;
import java.util.Scanner;

public class Codigo {
//lee el archivo y genera un codigo de enteros en la linea[0] en el archivo enviado por los parametros 
    public static int generarCodigo(String archivo) {
        int codigo = 0;
        try (Scanner datos = new Scanner(new File(archivo))) {
            while (datos.hasNextLine()) {
                String[] linea = datos.nextLine().split(",");
                codigo = Integer.parseInt(linea[0]);
            }
            return codigo + 1;

        } catch (Exception e) {
            System.out.println(e.getMessage());//manda un mensaje en caso de qu no se encuentre el archivo
        }
        return 1; //retorna un numero entero en caso de que haya una excepcion 
    }
}
