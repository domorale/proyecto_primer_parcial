package Utilidades;

import Datos.Usuario;
import Datos.Vendedor;
import Vehiculos.Vehiculo;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;

public class Venta {

    private int codVenta;
    private String cedula;
    private String placaVehiculo;
    private ArrayList<Oferta> ofertas;
    private Vehiculo vehiculo;
    private Vendedor vendedor;
//constructor de venta que recibe codigo de venta, la cedula del vendedor, la placa del vehiculo e inicializa la lista de ofertas
    public Venta(int codVenta, String cedula, String placaVehiculo) { 
        this.codVenta = codVenta;
        this.cedula = cedula;
        this.placaVehiculo = placaVehiculo;
        this.ofertas = new ArrayList<>();
    }
//sobrecarga del constructor venta que recibe solo el codigo de venta
    public Venta(int codVenta) {
        this.codVenta = codVenta;
    }
//funcion get del codigo de venta
    public int getCodVenta() {
        return codVenta;
    }
//funcionn set del codigo de venta
    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }
//funcion get de la cedula
    public String getCedula() {
        return cedula;
    }
//funcion set de la cedula
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
//funcion get de la placa de vehiculo
    public String getPlacaVehiculo() {
        return placaVehiculo;
    }
//funcion set de la placa del vehiculo
    public void setPlacaVehiculo(String placaVehiculo) {
        this.placaVehiculo = placaVehiculo;
    }
//funcion get de la lista oferta
    public ArrayList<Oferta> getOfertas() {
        return ofertas;
    }
//funcion set de la lista oferta
    public void setOfertas(ArrayList<Oferta> ofertas) {
        this.ofertas = ofertas;
    }
//funcion get del vehiculo 
    public Vehiculo getVehiculo() {
        return vehiculo;
    }
//funcion set del vehiculo
    public void setVehiculo(Vehiculo vehiculo) {
        this.vehiculo = vehiculo;
    }
//funcion get del vendedor
    public Vendedor getVendedor() {
        return vendedor;
    }
//funcion set del vendedor
    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }
//lee el archivo ventas.txt y agrega los valores de codigo venta, cedula, placa de vehiculo a una nueva lista llamada ventas
    public static ArrayList<Venta> leer_RegistroVentas() {
        ArrayList<Venta> ventas = new ArrayList<>();
        try (Scanner datos = new Scanner(new File("ventas.txt"))) {
            while (datos.hasNextLine()) {
                String[] linea = datos.nextLine().split(","); //almacena en la variable linea cada string que es separado  por coma
                int codVenta = Integer.parseInt(linea[0]);//transforma la linea[0] en entero y lo almacena en la variable codVenta
                String cedula = linea[1];// guarda la linea[1] en una variable cedula
                String placaVehiculo = linea[2];// guarda la linea[2] en la variable placaVehiculo
                ventas.add(new Venta(codVenta, cedula, placaVehiculo));// agrega las variables antes mencionadas en la lista ventas de Venta 
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ventas;//retorna la lista ventas
    }
//escribe en el archivo ventas.txt los atributos recibidos de ventas para registrar los datos de la misma
    public void registrar_venta() {
        try (FileWriter datos = new FileWriter("ventas.txt", true);//busca el fichero y en caso de que no exista lo crea 
                BufferedWriter bufer = new BufferedWriter(datos);//almacena una cantidad fija (hasta que este lleno), este escribe en FileWritter 
                PrintWriter agregar = new PrintWriter(bufer)) {//expone metodos adicionales como println, escribe en PrintWritter y este a su vez escribira en BufferedWriter 
            agregar.println(this);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }
//vincula las clases usuario,venta y vehiculo y se asegura de que no tire una excepcion, no retorna nada
    public static void vincular(ArrayList<Usuario> usuarios, ArrayList<Venta> listaVenta, ArrayList<Vehiculo> vehiculos) throws NoSuchAlgorithmException {
        for (Usuario user : usuarios) {//explora la lista usuarios
            if (user instanceof Vendedor) {//pregunta si el usuario es un Vendedor
                for (Venta r : listaVenta) {//recorre la lista de venta 
                    String placa = r.getPlacaVehiculo();//obtiene la placa del vehiculo 
                    String cedula = r.getCedula();// obtiene la cedula 
                    Vehiculo v = new Vehiculo(placa);//crea un nuevo vehiculo con la placa antes obtenida
                    v = vehiculos.get(vehiculos.indexOf(v)); //busca el indice del vehiculo y luego obtiene el mismo para almacenarlo en la nueva variable
                    Vendedor u = new Vendedor(cedula);//crea un nuevo vendedor con la cedula obtenida anteriormente
                    Vendedor vendedor = (Vendedor) user; //realiza un downcasting de usuario a vendedor
                    if (u.equals(user)) {//pregunta si los vendedores son iguales
                        v.getListaVentas().add(r);//agrega el objeto venta "r" a la lista de ventas del vehiculo
                        vendedor.getListaVenta().add(r); //agrega el objeto venta "r" a la lista venta del vendedor
                        r.setVehiculo(v); // cambia el vehiculo del objeto venta "r" por el vehisulo "v" creado con la placa antes obtenida
                        r.setVendedor(vendedor); //cambia el vendedor del objeto venta "r" por el vendedor creado anteriormente 
                    }

                }
            }

        }
    }
//da el formato en el que se quiere se impriman o se retornen los datos
    @Override
    public String toString() {
        return codVenta + "," + cedula + "," + placaVehiculo;
    }
//transforma los objetos en un entero unico 
    @Override
    public int hashCode() {
        int hash = 3;
        return hash;
    }
//compara los objetos para saber si son iguales
    @Override
    public boolean equals(Object obj) {
        if (this == obj) { 
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Venta other = (Venta) obj;

        return other.codVenta == this.codVenta; //retorna un booleano (True or False) sobre el atribito (codigo de venta) de ambos objetos
    }

}
