package Utilidades;

import Datos.Comprador;
import Datos.Usuario;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Scanner;

public class Oferta {

    private int codigoOferta;
    private int codVenta;
    private String cedula;
    private Comprador comprador;
    private Venta venta;
    private double cantidadOfertada;
//constructor de la clase oferta recibe sus atributos (codigoOferta,codVenta, cedula y la cantidadOfertada en formato double
    public Oferta(int codigoOferta, int codVenta, String cedula, double cantidadOfertada) {
        this.codigoOferta = codigoOferta;
        this.codVenta = codVenta;
        this.cedula = cedula;
        this.cantidadOfertada = cantidadOfertada;
    }
//sbrecarga al constructor para que solo reciba el atributo codigoOferta
    public Oferta(int codigoOferta) {
        this.codigoOferta = codigoOferta;
    }
//funcion get del codigo oferta
    public int getCodigoOferta() {
        return codigoOferta;
    }
//funcion set del codigo oferta
    public void setCodigoOferta(int codigoOferta) {
        this.codigoOferta = codigoOferta;
    }
//funcion get del codigo venta
    public int getCodVenta() {
        return codVenta;
    }
//funcion set del codigo venta
    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }
//funcion get de la cedula
    public String getCedula() {
        return cedula;
    }
//funcion set de la cedula
    public void setCedula(String cedula) {
        this.cedula = cedula;
    }
//funcion get del comprador
    public Comprador getComprador() {
        return comprador;
    }
//funcion set del comprador
    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }
//funcion get de venta
    public Venta getVenta() {
        return venta;
    }
//funcion set de venta
    public void setVenta(Venta venta) {
        this.venta = venta;
    }
//funcion get de cantidad ofertada
    public double getCantidadOfertada() {
        return cantidadOfertada;
    }
//funcion set de cantidad ofertada
    public void setCantidadOfertada(double cantidadOfertada) {
        this.cantidadOfertada = cantidadOfertada;
    }
//lee el archivo ofertas.txt y agrega los valores codOferta,codVenta,cedula, canttidadOfertada a la lista ofertas creada y procede a retornarla
    public static ArrayList<Oferta> leer_RegistroOfertas() {
        ArrayList<Oferta> ofertas = new ArrayList<>();
        try (Scanner datos = new Scanner(new File("ofertas.txt"))) {
            while (datos.hasNextLine()) {
                String[] linea = datos.nextLine().split(",");
                int codOferta = Integer.parseInt(linea[0]);
                int codVenta = Integer.parseInt(linea[1]);
                String cedula = linea[2];
                double cantidadOfertada = Double.parseDouble(linea[3]);
                ofertas.add(new Oferta(codOferta, codVenta, cedula, cantidadOfertada));
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return ofertas;
    }
//escribe en el archivo ofertas.txt los atributos recibidos de ventas para registrar los datos de la misma
    public void registrar_oferta() {
        try (FileWriter datos = new FileWriter("ofertas.txt", true);
                BufferedWriter bufer = new BufferedWriter(datos);
                PrintWriter agregar = new PrintWriter(bufer)) {
            agregar.println(this);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
//vincula las clases usuario, oferta, venta y se asegura que no haya excepciones, no retorna nada
    public static void vincular(ArrayList<Usuario> usuarios, ArrayList<Oferta> listaOfertas, ArrayList<Venta> venta) throws NoSuchAlgorithmException {
        for (Usuario user : usuarios) {
            if (user instanceof Comprador) { //pregunta si el usuario pertenece a la clase comprador
                for (Oferta o : listaOfertas) { //recorrela lista ofertas
                    int codigo = o.getCodVenta();
                    String cedula = o.getCedula();
                    Venta v = new Venta(codigo);
                    v = venta.get(venta.indexOf(v));
                    Comprador c = new Comprador(cedula);
                    Comprador comprador = (Comprador) user;
                    if (c.equals(user)) { //pregunta si ambos compradores son iguales 
                        v.getOfertas().add(o);
                        comprador.getListaOferta().add(o);
                        o.setVenta(v);
                        o.setComprador(comprador);
                    }
                }
            }
        }
    }
//da el formato en el que se quiere se impriman o se retornen los datos
    @Override
    public String toString() {
        return codigoOferta + "," + codVenta + "," + cedula + "," + cantidadOfertada;
    }

}
