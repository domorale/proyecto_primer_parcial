/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import java.util.Scanner;

/**
 *
 * @author dilan
 */
public class Validar {
//valida que el datos ingresados del usuario sean letras y que las palabras esten separadas por un espacio y los retorna
    public static String validarAlphaUsuario(String dato, Scanner ingreso) {
        int contador = 0;
        do {
            if (contador > 0) {
                System.out.println("ingrese un dato correcto");
                dato = ingreso.next();
            }
            contador++;
        } while (!dato.matches("[A-Za-z]*+[\\s]+[A-Za-z]*"));

        return dato;
    }
//valida que los datos sean letras y los retorna 
    public static String validarAlph(String dato, Scanner ingreso) {
        int contador = 0;
        do {
            if (contador > 0) {
                System.out.println("ingrese un dato correcto");
                dato = ingreso.next();
            }
            contador++;
        } while (!dato.matches("[A-Za-z]*"));

        return dato;
    }
//valida que los datos ingresados sean numeros enteros y procede a retornarlos en forma de string
    public static String validarDigit(String dato, Scanner ingreso) {
        int contador = 0;
        do {
            if (contador > 0) {
                System.out.println("ingrese un dato correcto");
                dato = ingreso.next();
            }
            contador++;
        } while (!dato.matches("[+-]?[0-9][0-9]*"));

        return dato;
    }
//valida que los datos ingresados sean doubles y procede a retornarlos como string
    public static String validarDouble(String dato, Scanner ingreso) {
        int contador = 0;
        do {
            if (contador > 0) {
                System.out.println("ingrese un dato correcto");
                dato = ingreso.next();
            }
            contador++;
        } while (!dato.matches("[+-]?[0-9]+(\\.[0-9]+)?([Ee][+-]?[0-9]+)?"));

        return dato;
    }
//valida que el correo ingresado contenga el "@" y que termine en ".com" y procede a retornarlo
    public static String validarCorreo(String correo, Scanner ingreso) {
        int contador = 0;
        do {
            if (contador > 0) {
                System.out.println("Correo incorrecto, vuelva a ingresar");
                correo = ingreso.next();
            }
            contador++;
        } while (!(correo.contains("@") && correo.contains(".com")));
        return correo;
    }

}
