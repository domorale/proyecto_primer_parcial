package Main;

import Datos.Comprador;
import Datos.Usuario;
import Datos.Vendedor;
import Utilidades.Buscar;
import Utilidades.Correo;
import Utilidades.Encriptar;
import Utilidades.Validar;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Scanner;
import Utilidades.Oferta;
import Utilidades.Venta;
import Vehiculos.Auto;
import Vehiculos.Camion;
import Vehiculos.Camioneta;
import Vehiculos.Motocicleta;
import Vehiculos.Vehiculo;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        ArrayList<Usuario> usuarios;
        ArrayList<Oferta> ofertas;
        ArrayList<Venta> ventas;
        ArrayList<Vehiculo> vehiculos;
        Scanner entrada = new Scanner(System.in);
        entrada.useLocale(Locale.US);
        entrada.useDelimiter("\n");
        String opcion = "0";
        while (!opcion.equals("3")) {
            usuarios = Usuario.leer_RegistroUsuario();
            ofertas = Oferta.leer_RegistroOfertas();
            ventas = Venta.leer_RegistroVentas();
            vehiculos = Vehiculo.leer_RegistroVechiculos();
            Venta.vincular(usuarios, ventas, vehiculos);
            Oferta.vincular(usuarios, ofertas, ventas);
            System.out.println("Menú de Opciones: \n" + "\n" + "1. Vendedor \n" + "2. Comprador\n" + "3. Salir");
            opcion = entrada.next();
            switch (opcion) {
                case "1": // OPCION 1. VENDEDOR
                    while (!opcion.equals("4")) {
                        usuarios = Usuario.leer_RegistroUsuario();
                        ofertas = Oferta.leer_RegistroOfertas();
                        ventas = Venta.leer_RegistroVentas();
                        vehiculos = Vehiculo.leer_RegistroVechiculos();
                        Venta.vincular(usuarios, ventas, vehiculos);
                        Oferta.vincular(usuarios, ofertas, ventas);
                        System.out.println("1. Registrar un nuevo vendedor\n" + "2. Ingresar un nuevo vehículo\n"
                                + "3. Aceptar oferta\n" + "4. Regresar");
                        opcion = entrada.next();
                        switch (opcion) {
                            case "1": // OPCION 1 "REGISTRAR VENDEDOR" DE MENU 1. VENDEDOR
                                System.out.println("Ingrese sus nombres:");
                                String nombres = entrada.next();
                                nombres = Validar.validarAlphaUsuario(nombres, entrada).replace(" ", "");
                                System.out.println("Ingrese sus apellidos:");
                                String apellidos = entrada.next();
                                apellidos = Validar.validarAlphaUsuario(apellidos, entrada).replace(" ", "");
                                System.out.println("Ingrese su cedula:");
                                String cedula = entrada.next();
                                cedula = Validar.validarDigit(cedula, entrada);
                                System.out.println("Ingrese su organización:");
                                String organizacion = entrada.next();
                                String correo;
                                String usuario;
                                String placa;
                                int contador = 0;
                                do {
                                    if (contador > 0) {
                                        System.out.println("Usuario o Correo registrado, ingrese otro por favor.");
                                    }
                                    contador++;
                                    System.out.println("Ingrese usuario:");
                                    usuario = entrada.next();
                                    System.out.println("Ingrese su correo:");
                                    correo = entrada.next();
                                    correo = Validar.validarCorreo(correo, entrada);
                                } while (Buscar.comprobar_usuario(correo, usuario));
                                System.out.println("Ingresar contraseña:");
                                String contraseña = entrada.next();
                                Vendedor vendedor = new Vendedor(cedula, nombres, apellidos, correo, organizacion, usuario, contraseña);
                                vendedor.registro_usuario();
                                System.out.println("USUARIO REGISTRADO");
                                break; // FINALIZA OPCION 1 "REGISTRAR VENDEDOR" DE MENU 1. VENDEDOR

                            case "2": // OPCION 2 "REGISTRAR VEHICULO" DE MENU 1. VENDEDOR
                                contador = 0;
                                usuarios = Usuario.leer_RegistroUsuario();
                                ofertas = Oferta.leer_RegistroOfertas();
                                ventas = Venta.leer_RegistroVentas();
                                vehiculos = Vehiculo.leer_RegistroVechiculos();
                                Venta.vincular(usuarios, ventas, vehiculos);
                                Oferta.vincular(usuarios, ofertas, ventas);
                                do {
                                    if (contador > 0) {
                                        System.out.println("Usuario no registrado o contraseña incorrecta");
                                    }
                                    System.out.println("Ingrese su cedula y contraseña");
                                    System.out.println("Cedula:");
                                    cedula = entrada.next();
                                    System.out.println("Contraseña:");
                                    contraseña = entrada.next();
                                    contraseña = Encriptar.toHexString(Encriptar.getSHA(contraseña));
                                    contador++;
                                } while (!Buscar.comprobar_usuario_BD(contraseña, cedula));
                                Usuario user = new Vendedor(cedula);
                                Vendedor vendedor_1 = (Vendedor) usuarios.get(usuarios.indexOf(user));
                                String tipo_vehiculo = "";
                                Vehiculo vehiculo = null;
                                do {
                                    System.out.println("Escoja la opción de vehiculo a ingresar:\n" + "1. Auto\n" + "2. Camión\n" + "3. Camioneta\n"
                                            + "4. Motocicleta");
                                    tipo_vehiculo = entrada.next();
                                    if (tipo_vehiculo.equals("1") || tipo_vehiculo.equals("2") || tipo_vehiculo.equals("3")
                                            || tipo_vehiculo.equals("4")) {
                                        System.out.println("Ingrese la placa del vehiculo:");
                                        do {
                                            contador = 0;
                                            if (contador > 0) {
                                                System.out.print("Vehiculo registrado, ingrese otro numero de placa");
                                            }
                                            placa = entrada.next();
                                            contador++;
                                        } while (Buscar.verificarVehiculo(new Vehiculo(placa)));
                                        System.out.println("Ingrese la marca del vehiculo:");
                                        String marca = entrada.next();
                                        marca = Validar.validarAlph(marca, entrada);
                                        System.out.println("Ingrese el modelo del vehiculo:");
                                        String modelo = entrada.next();

                                        System.out.println("Ingrese el tipo de motor:");
                                        String tipo_motor = entrada.next();
                                        tipo_motor = Validar.validarAlph(tipo_motor, entrada);
                                        System.out.println("Ingrese el año del vehiculo:");
                                        String año = entrada.next();
                                        año = Validar.validarDigit(año, entrada);
                                        System.out.println("Ingrese el recorrido del vehiculo:");
                                        String recorrido = entrada.next();
                                        recorrido = Validar.validarDouble(recorrido, entrada);
                                        System.out.println("Ingrese el color del vehiculo:");
                                        String color = entrada.next();
                                        color = Validar.validarAlph(color, entrada);
                                        System.out.println("Ingrese el tipo de combustible del vehiculo:");
                                        String tipo_combustible = entrada.next();
                                        tipo_combustible = Validar.validarAlph(tipo_combustible, entrada);
                                        System.out.println("Ingrese el tipo de transmición del vehiculo:");
                                        String transmicion = entrada.next();
                                        transmicion = Validar.validarAlph(transmicion, entrada);
                                        System.out.println("Ingrese el precio del vehiculo:");
                                        String precio = entrada.next();
                                        precio = Validar.validarDouble(precio, entrada);
                                        switch (tipo_vehiculo) {
                                            case "1":
                                                vehiculo = new Auto(placa, marca, modelo, tipo_motor, Integer.parseInt(año), Double.parseDouble(recorrido),
                                                        color, tipo_combustible, transmicion, Double.parseDouble(precio));
                                                vendedor_1.registrarVenta(vehiculo);
                                                break;
                                            case "2":
                                                vehiculo = new Camion(placa, marca, modelo, tipo_motor, Integer.parseInt(año), Double.parseDouble(recorrido),
                                                        color, tipo_combustible, transmicion, Double.parseDouble(precio));
                                                vendedor_1.registrarVenta(vehiculo);
                                                break;
                                            case "3":
                                                vehiculo = new Camioneta(placa, marca, modelo, tipo_motor, Integer.parseInt(año), Double.parseDouble(recorrido),
                                                        color, tipo_combustible, transmicion, Double.parseDouble(precio));
                                                vendedor_1.registrarVenta(vehiculo);
                                                break;
                                            case "4":
                                                vehiculo = new Motocicleta(placa, marca, modelo, tipo_motor, Integer.parseInt(año), Double.parseDouble(recorrido),
                                                        color, tipo_combustible, transmicion, Double.parseDouble(precio));
                                                vendedor_1.registrarVenta(vehiculo);
                                                break;
                                            default:
                                                break;
                                        }
                                    } else {
                                        System.out.println("Ingrese una opción valida");
                                    }
                                } while (!Buscar.verificarVehiculo(vehiculo));
                                break; // FIN DE  OPCION 2 "REGISTRAR VEHICULO" DE MENU 1. VENDEDOR

                            case "3": // OPCION 3 "REVISAR OFERTAS" DE MENU 1. VENDEDOR
                                contador = 0;
                                usuarios = Usuario.leer_RegistroUsuario();
                                ofertas = Oferta.leer_RegistroOfertas();
                                ventas = Venta.leer_RegistroVentas();
                                vehiculos = Vehiculo.leer_RegistroVechiculos();
                                Venta.vincular(usuarios, ventas, vehiculos);
                                Oferta.vincular(usuarios, ofertas, ventas);
                                do {
                                    if (contador > 0) {
                                        System.out.println("Usuario no registrado o contraseña incorrecta");
                                    }
                                    System.out.println("Ingrese su usuario y contraseña");
                                    System.out.println("Cedula:");
                                    cedula = entrada.next();
                                    System.out.println("Contraseña:");
                                    contraseña = entrada.next();
                                    contraseña = Encriptar.toHexString(Encriptar.getSHA(contraseña));
                                    contador++;
                                } while (!Buscar.comprobar_usuario_BD(contraseña, cedula));
                                Usuario c = new Vendedor(cedula);
                                vendedor_1 = (Vendedor) usuarios.get(usuarios.indexOf(c));

                                ArrayList<Venta> lista_ventas = vendedor_1.getListaVenta();
                                System.out.println("Ingrese la placa del vehiculo:");
                                do {
                                    contador = 0;
                                    if (contador > 0) {
                                        System.out.println("Placa incorrecta, ingrese otra vez");
                                    }
                                    placa = entrada.next();
                                    vehiculo = new Vehiculo(placa);
                                } while (!Buscar.verificarVehiculo(vehiculo));

                                ArrayList<Oferta> o = Buscar.Ofertas_de_vendedor(vendedor_1, vehiculo);
                                if (o.size() == 0) {
                                    System.out.println("No existen ofertas");
                                    break;
                                }
                                Vehiculo v = vehiculos.get(vehiculos.indexOf(vehiculo));
                                System.out.println(v.getMarca() + " " + v.getModelo() + " " + v.getPrecio());
                                System.out.println("Cantidad de ofertas: " + o.size());
                                int posicion = 0;
                                do {
                                    Oferta oferta;
                                    String asunto = "OFERTA DE VIHICULO ACEPTADA";

                                    if (posicion == 0 && o.size() == 1) {
                                        System.out.println("*********OFERTAS*************");
                                        oferta = o.get(posicion);
                                        System.out.println("Correo: " + oferta.getComprador().getCorreoE());
                                        System.out.println("Cantidad ofertada: " + oferta.getCantidadOfertada());
                                        System.out.println("1. Salir\n" + "2. Aceptar Oferta");
                                        opcion = entrada.next();
                                        if (opcion.equals("2")) {
                                            System.out.println("OFERTA ACEPTADA");
                                            String correo_comprador = oferta.getComprador().getCorreoE();
                                            String mensaje = "Felecidades " + oferta.getComprador().getNombre() + " tu oferta por el vehiculo: \n"
                                                    + v.getMarca() + " " + v.getModelo() + " fue aceptada";
                                            Correo.enviarConGMail(correo_comprador, asunto,
                                                    mensaje);
                                            System.out.println("SE ACABA DE NOTIFICAR AL COMPRADOR");
                                            vehiculos.remove(vehiculos.get(vehiculos.indexOf(vehiculo)));

                                        }

                                    }
                                    if (posicion == 0 && (o.size() > 1)) {
                                        System.out.println("*********OFERTAS*************");
                                        oferta = o.get(posicion);
                                        System.out.println("Correo: " + oferta.getComprador().getCorreoE());
                                        System.out.println("Cantidad ofertada: " + oferta.getCantidadOfertada());
                                        System.out.println("1. Salir\n" + "2. Siguiente\n" + "3. Aceptar oferta");
                                        opcion = entrada.next();
                                        switch (opcion) {
                                            case "2":
                                                posicion++;
                                                break;
                                            case "3":
                                                System.out.println("OFERTA ACEPTADA");
                                                String correo_comprador = oferta.getComprador().getCorreoE();
                                                String mensaje = "Felecidades " + oferta.getComprador().getNombre() + " tu oferta por el vehiculo: \n"
                                                        + v.getMarca() + " " + v.getModelo() + " fue aceptada";
                                                Correo.enviarConGMail(correo_comprador, asunto,
                                                        mensaje);
                                                System.out.println("SE ACABA DE NOTIFICAR AL COMPRADOR");
                                                vehiculos.remove(vehiculos.get(vehiculos.indexOf(vehiculo)));
                                                break;

                                        }
                                    }
                                    if (posicion > 0 && !(posicion + 1 == o.size())) {
                                        System.out.println("*********OFERTAS*************");
                                        oferta = o.get(posicion);
                                        System.out.println("Correo: " + oferta.getComprador().getCorreoE());
                                        System.out.println("Cantidad ofertada: " + oferta.getCantidadOfertada());
                                        System.out.println("1. Salir\n" + "2. Siguiente\n" + "3. Atras\n" + "4. Aceptar oferta");
                                        opcion = entrada.next();
                                        switch (opcion) {
                                            case "2":
                                                posicion++;
                                                break;
                                            case "3":
                                                posicion--;
                                                break;
                                            case "4":
                                                System.out.println("OFERTA ACEPTADA");
                                                String correo_comprador = oferta.getComprador().getCorreoE();
                                                String mensaje = "Felecidades " + oferta.getComprador().getNombre() + " tu oferta por el vehiculo: \n"
                                                        + v.getMarca() + " " + v.getModelo() + " fue aceptada";
                                                Correo.enviarConGMail(correo_comprador, asunto,
                                                        mensaje);
                                                System.out.println("SE ACABA DE NOTIFICAR AL COMPRADOR");
                                                vehiculos.remove(vehiculos.get(vehiculos.indexOf(vehiculo)));
                                                break;
                                        }

                                    }
                                    if (posicion + 1 == o.size() && (o.size() != 1)) {
                                        System.out.println("*********OFERTAS*************");
                                        oferta = o.get(posicion);
                                        System.out.println("Correo: " + oferta.getComprador().getCorreoE());
                                        System.out.println("Cantidad ofertada: " + oferta.getCantidadOfertada());
                                        System.out.println("1. Salir\n" + "2. Atras\n" + "3. Aceptar oferta");
                                        opcion = entrada.next();
                                        switch (opcion) {
                                            case "2":
                                                posicion--;
                                                break;
                                            case "3":
                                                System.out.println("OFERTA ACEPTADA");
                                                String correo_comprador = oferta.getComprador().getCorreoE();
                                                String mensaje = "Felecidades " + oferta.getComprador().getNombre() + " tu oferta por el vehiculo: \n"
                                                        + v.getMarca() + " " + v.getModelo() + " fue aceptada";
                                                Correo.enviarConGMail(correo_comprador, asunto,
                                                        mensaje);
                                                System.out.println("SE ACABA DE NOTIFICAR AL COMPRADOR");
                                                vehiculos.remove(vehiculos.get(vehiculos.indexOf(vehiculo)));
                                                break;

                                        }
                                    }

                                } while (!"1".equals(opcion));
                                break; //  FIN DE LA OPCION 3 "REVISAR OFERTAS" DE MENU 1. VENDEDOR
                        }

                    }
                    break; // BREAK DEL OPCION 1 (VENDEDOR)

                case "2": // OPCION 2. COMPRADOR
                    usuarios = Usuario.leer_RegistroUsuario();
                    ofertas = Oferta.leer_RegistroOfertas();
                    ventas = Venta.leer_RegistroVentas();
                    vehiculos = Vehiculo.leer_RegistroVechiculos();
                    Venta.vincular(usuarios, ventas, vehiculos);
                    Oferta.vincular(usuarios, ofertas, ventas);
                    while (!opcion.equals("3")) {
                        System.out.println("1. Registrar un nuevo comprador\n" + "2. Realizar una oferta\n"
                                + "3. Regresar");
                        opcion = entrada.next();
                        switch (opcion) {

                            case "1": // OPCION  "1 .REGISTRAR COMPRADOR" DEL MENU COMPRADOR
                                System.out.println("Ingrese sus nombres");
                                String nombres = entrada.next();
                                nombres = Validar.validarAlphaUsuario(nombres, entrada).replace(" ", "");
                                System.out.println("Ingrese sus apellidos");
                                String apellidos = entrada.next();
                                apellidos = Validar.validarAlphaUsuario(apellidos, entrada).replace(" ", "");
                                System.out.println("Ingrese su cedula");
                                String cedula = entrada.next();
                                cedula = Validar.validarDigit(cedula, entrada);
                                System.out.println("Ingrese su organización");
                                String organizacion = entrada.next();
                                String correo;
                                String usuario;
                                int contador = 0;
                                do {
                                    if (contador > 0) {
                                        System.out.println("Usuario o Correo registrado, ingrese otro por favor");
                                    }
                                    contador++;
                                    System.out.println("Ingrese usuario");
                                    usuario = entrada.next();
                                    System.out.println("Ingrese su correo");
                                    correo = entrada.next();
                                    correo = Validar.validarCorreo(correo, entrada);
                                } while (Buscar.comprobar_usuario(correo, usuario));
                                System.out.println("Ingresar contraseña");
                                String contraseña = entrada.next();
                                Comprador comprador = new Comprador(cedula, nombres, apellidos, correo, organizacion, usuario, contraseña);
                                comprador.registro_usuario();
                                System.out.println("USUARIO REGISTRADO");
                                opcion = "0";
                                break;// FIN DE LA OPCION  "1 .REGISTRAR COMPRADOR" DEL MENU COMPRADOR

                            case "2":// OPCION "2. REALIZAR OFERTA" DEL MENU 2. COMPRADOR
                                contador = 0;
                                usuarios = Usuario.leer_RegistroUsuario();
                                ofertas = Oferta.leer_RegistroOfertas();
                                ventas = Venta.leer_RegistroVentas();
                                vehiculos = Vehiculo.leer_RegistroVechiculos();
                                Venta.vincular(usuarios, ventas, vehiculos);
                                Oferta.vincular(usuarios, ofertas, ventas);
                                do {
                                    if (contador > 0) {
                                        System.out.println("Usuario no registrado o contraseña incorrecta");
                                    }
                                    System.out.println("Ingrese su usuario y contraseña");
                                    System.out.println("Cedula:");
                                    cedula = entrada.next();
                                    System.out.println("Contraseña:");
                                    contraseña = entrada.next();
                                    contraseña = Encriptar.toHexString(Encriptar.getSHA(contraseña));
                                    contador++;
                                } while (!Buscar.comprobar_usuario_BD(contraseña, cedula));
                                Usuario comprador_1 = new Comprador(cedula);
                                Venta.vincular(usuarios, ventas, vehiculos);
                                Oferta.vincular(usuarios, ofertas, ventas);
                                comprador = (Comprador) usuarios.get(usuarios.indexOf(comprador_1));
                                System.out.println("Elija el tipo de vehiculo que quiere buscar");
                                String tipo_vehiculo;
                                do {
                                    contador = 0;
                                    if (contador > 0) {
                                        System.out.println("Ingrese una opcion correcta");
                                    }
                                    System.out.println("Ingrese el tipo de vehiculo:\n" + "Auto\n" + "Camión\n" + "Camioneta\n"
                                            + "Motocicleta");
                                    tipo_vehiculo = entrada.next();
                                } while (!(tipo_vehiculo.equals("Auto") || tipo_vehiculo.equals("Camión") || tipo_vehiculo.equals("Camioneta")
                                        || tipo_vehiculo.equals("Motocicleta")));
                                System.out.println("Ingrese el recorrido minimo");
                                String recorrido_min = entrada.next();
                                recorrido_min = Validar.validarDouble(recorrido_min, entrada);
                                System.out.println("Ingrese el recorrido maximo");
                                String recorrido_max = entrada.next();
                                recorrido_min = Validar.validarDouble(recorrido_min, entrada);
                                System.out.println("Ingrese el año minimo");
                                String año_min = entrada.next();
                                año_min = Validar.validarDigit(año_min, entrada);
                                System.out.println("Ingrese el año maximo");
                                String año_max = entrada.next();
                                año_min = Validar.validarDigit(año_max, entrada);
                                System.out.println("Ingrese el precio minimo");
                                String precio_min = entrada.next();
                                precio_min = Validar.validarDouble(precio_min, entrada);
                                System.out.println("Ingrese el precio maximo");
                                String precio_max = entrada.next();
                                precio_max = Validar.validarDouble(precio_max, entrada);
                                ArrayList<Venta> vehiculos_venta = Buscar.buscar_vehiculo(ventas, tipo_vehiculo, Double.parseDouble(recorrido_min),
                                        Double.parseDouble(recorrido_max), Integer.parseInt(año_min), Integer.parseInt(año_max), Double.parseDouble(precio_min), Double.parseDouble(precio_max));
                                int posicion = 0;
                                do {
                                    Vehiculo vehiculo;
                                    if (vehiculos_venta.size() == 0) {

                                        opcion = "1";

                                    }
                                    if (posicion == 0 && vehiculos_venta.size() == 1) {
                                        System.out.println("*********EN VENTA*************");
                                        vehiculo = vehiculos_venta.get(posicion).getVehiculo();
                                        System.out.println("Precio: " + vehiculo.getPrecio() + "\n" + "Marca: " + vehiculo.getMarca() + "\n"
                                                + "Modelo: " + vehiculo.getModelo() + "\n" + "Color: " + vehiculo.getColor() + "\n" + "Tipo de combustible: " + vehiculo.getTipoCombustible()
                                                + "\n" + "Tipo de motor: " + vehiculo.getTipoMotor()
                                                + "\n" + "Tipo de transmición: " + vehiculo.getTransmision());
                                        System.out.println("1. Salir\n" + "2. Ofertar");
                                        opcion = entrada.next();
                                        if (opcion.equals("2")) {
                                            System.out.println("Ingrese la cantidad a ofertar");
                                            String dinero = entrada.next();
                                            dinero = Validar.validarDouble(dinero, entrada);
                                            comprador.realizarOferta(vehiculos_venta.get(posicion), Double.parseDouble(dinero));
                                            System.out.println("OFERTA REALIZADA");
                                        }
                                    }
                                    if (posicion == 0 && (vehiculos_venta.size() > 1)) {
                                        System.out.println("*********EN VENTA*************");
                                        vehiculo = vehiculos_venta.get(posicion).getVehiculo();
                                        System.out.println("Precio: " + vehiculo.getPrecio() + "\n" + "Marca: " + vehiculo.getMarca() + "\n"
                                                + "Modelo: " + vehiculo.getModelo() + "\n" + "Color: " + vehiculo.getColor() + "\n" + "Tipo de combustible: " + vehiculo.getTipoCombustible()
                                                + "\n" + "Tipo de motor: " + vehiculo.getTipoMotor()
                                                + "\n" + "Tipo de transmición: " + vehiculo.getTransmision());
                                        System.out.println("1. Salir\n" + "2. Siguiente\n" + "3. Ofertar");
                                        opcion = entrada.next();
                                        if (opcion.equals("2")) {
                                            posicion = posicion + 1;
                                        }
                                        if (opcion.equals("3")) {
                                            System.out.println("Ingrese la cantidad a ofertar");
                                            String dinero = entrada.next();
                                            dinero = Validar.validarDouble(dinero, entrada);
                                            comprador.realizarOferta(vehiculos_venta.get(posicion), Double.parseDouble(dinero));
                                            System.out.println("OFERTA REALIZADA");

                                        }
                                    }
                                    if (posicion > 0 && !(posicion + 1 == vehiculos_venta.size())) {
                                        System.out.println("*********EN VENTA*************");
                                        vehiculo = vehiculos_venta.get(posicion).getVehiculo();
                                        System.out.println("Precio: " + vehiculo.getPrecio() + "\n" + "Marca: " + vehiculo.getMarca() + "\n"
                                                + "Modelo: " + vehiculo.getModelo() + "\n" + "Color: " + vehiculo.getColor() + "\n" + "Tipo de combustible: " + vehiculo.getTipoCombustible()
                                                + "\n" + "Tipo de motor: " + vehiculo.getTipoMotor()
                                                + "\n" + "Tipo de transmición: " + vehiculo.getTransmision());
                                        System.out.println("1. Salir\n" + "2. Siguiente\n" + "3. Atras\n" + "4. Ofertar");
                                        opcion = entrada.next();
                                        switch (opcion) {
                                            case "2":
                                                posicion++;
                                                break;
                                            case "3":
                                                posicion--;
                                                break;
                                            case "4":
                                                System.out.println("Ingrese la cantidad a ofertar");
                                                String dinero = entrada.next();
                                                dinero = Validar.validarDouble(dinero, entrada);
                                                comprador.realizarOferta(vehiculos_venta.get(posicion), Double.parseDouble(dinero));
                                                System.out.println("OFERTA REALIZADA");
                                                break;
                                        }

                                    }
                                    if (posicion + 1 == vehiculos_venta.size() && (vehiculos_venta.size() != 1)) {
                                        System.out.println("*********EN VENTA*************");
                                        vehiculo = vehiculos_venta.get(posicion).getVehiculo();
                                        System.out.println("Precio: " + vehiculo.getPrecio() + "\n" + "Marca: " + vehiculo.getMarca() + "\n"
                                                + "Modelo: " + vehiculo.getModelo() + "\n" + "Color: " + vehiculo.getColor() + "\n" + "Tipo de combustible: " + vehiculo.getTipoCombustible()
                                                + "\n" + "Tipo de motor: " + vehiculo.getTipoMotor()
                                                + "\n" + "Tipo de transmición: " + vehiculo.getTransmision());
                                        System.out.println("1. Salir\n" + "2. Atras\n" + "3. Ofertar");
                                        opcion = entrada.next();
                                        switch (opcion) {
                                            case "2":
                                                posicion--;
                                                break;
                                            case "3":
                                                System.out.println("Ingrese la cantidad a ofertar");
                                                String dinero = entrada.next();
                                                dinero = Validar.validarDouble(dinero, entrada);
                                                comprador.realizarOferta(vehiculos_venta.get(posicion), Double.parseDouble(dinero));
                                                System.out.println("OFERTA REALIZADA");
                                                break;
                                        }
                                    }

                                } while (!"1".equals(opcion));

                                break; // FIN DE LAOPCION "2. REALIZAR OFERTA" DEL MENU 2. COMPRADOR
                        }
                        opcion = "0";
                        break;// FIN DE LA OPCION 2. COMPRADOR

                    }
                case "0":
                    break;
                default:
                    System.out.println("Ingrese una opcion valida");
                    break;
            } /// FIN DE LOS MENUS

        }

        entrada.close();
    }
}
