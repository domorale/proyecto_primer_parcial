package Datos;

import Utilidades.Encriptar;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Usuario {

    protected String nombre;
    protected String apellido;
    protected String cedula;
    protected String correoE;
    protected String organizacion;
    protected String usuario;
    protected String clave;
    protected boolean comprador;
    protected boolean vendedor;

    public Usuario(String cedula, String nombre, String apellido, String correoE, String organizacion, String usuario, String clave, boolean comprador, boolean vendedor) throws NoSuchAlgorithmException {
        this.nombre = nombre.toUpperCase();
        this.apellido = apellido.toUpperCase();
        this.cedula = cedula;
        this.correoE = correoE;
        this.organizacion = organizacion;
        this.usuario = usuario;
        this.clave = Encriptar.toHexString(Encriptar.getSHA(clave));
        this.comprador = comprador;
        this.vendedor = vendedor;
    }

    public Usuario(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreoE() {
        return correoE;
    }

    public void setCorreoE(String correoE) {
        this.correoE = correoE;
    }

    public String getOrganizacion() {
        return organizacion;
    }

    public void setOrganizacion(String organizacion) {
        this.organizacion = organizacion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    //public abstract ArrayList<Oferta> getListaOferta();
    //public abstract ArrayList<Venta> getListaVenta();

    public void registro_usuario() {
        try (FileWriter datos = new FileWriter("usuarios.txt", true); //Se utilizara el FileWriter para escribir el archivo usuarios.txt
                BufferedWriter bufer = new BufferedWriter(datos); //Se define un objeto de tipo BufferedWriter que recibe el archivo datos
                PrintWriter agregar = new PrintWriter(bufer)) { //Definimos un objeto de tipo PrintWriter llamado agregar que recibe a bufer y aqui se procede a escribir el archivo
            agregar.println(this);
        } catch (IOException ex) { // Si el try no puede realizar lo mencionado previamente, el catch mostrara un mensaje explicando que no fue posible realizar la escritura del archivo.
            System.out.println(ex.getMessage());
        }
    }

    public static ArrayList<Usuario> leer_RegistroUsuario() {
        ArrayList<Usuario> usuarios = new ArrayList<>(); //Creamos una lista de tipo Usario
        try (Scanner datos = new Scanner(new File("usuarios.txt"))) { //Utilizamos el scanner para leer datos
            while (datos.hasNextLine()) { //Mientras el objeto datos tenga una linea siguiente, se procedera a realizar lo que esta a continuacion
                String[] linea = datos.nextLine().replace("\\s+", "").split(",");//Se define un arreglo de tipo String haciendo el split de datos donde haya coma
                if (linea[7].equals("true")) {//Si ese arreglo en su indice 7 es igual a true se realiza lo siguiente
                    String nombre = linea[1];//Se define la variable nombre de tipo String sacando el elemento de linea con indice 1
                    String apellido = linea[2];//Se define la variable apellido de tipo String sacando el elemento de linea con indice 2
                    String cedula = linea[0];//Se define la variable cedula de tipo String sacando el elemento de linea con indice 0
                    String correoE = linea[3];//Se define la variable correoE de tipo String sacando el elemento de linea con indice 3
                    String organizacion = linea[4];//Se define la variable organizacion de tipo String sacando el elemento de linea con indice 4
                    String usuario = linea[5];//Se define la variable usuario de tipo String sacando el elemento de linea con indice 5
                    String clave = linea[6];//Se define la variable clave de tipo String sacando el elemento de linea con indice 6
                    usuarios.add(new Comprador(cedula, nombre, apellido, correoE, organizacion, usuario, clave));//Se crea un objeto Comprador con los datos anteriores y se lo agrega a la lista usuarios
                } else {//Si ese arreglo en su indice 7 no es igual a true se realiza lo siguiente
                    String nombre = linea[1];//Se define la variable nombre de tipo String sacando el elemento de linea con indice 1
                    String apellido = linea[2];//Se define la variable apellido de tipo String sacando el elemento de linea con indice 2
                    String cedula = linea[0];//Se define la variable cedula de tipo String sacando el elemento de linea con indice 0
                    String correoE = linea[3];//Se define la variable correoE de tipo String sacando el elemento de linea con indice 3
                    String organizacion = linea[4];//Se define la variable organizacion de tipo String sacando el elemento de linea con indice 4
                    String usuario = linea[5];//Se define la variable usuario de tipo String sacando el elemento de linea con indice 5
                    String clave = linea[6];//Se define la variable clave de tipo String sacando el elemento de linea con indice 6
                    usuarios.add(new Vendedor(cedula, nombre, apellido, correoE, organizacion, usuario, clave));//Se crea un objeto Vendedor con los datos anteriores y se lo agrega a la lista usuarios
                }

            }
        } catch (Exception e) { //En caso de que no se pueda realizar lo que esta dentro del try, el catch arroja un mensaje avisando que no fue posible
            System.out.println(e.getMessage());
        }
        return usuarios;//Se retorna la lista usuarios
    }

    @Override
    public String toString() {
        return cedula + "," + nombre + "," + apellido + "," + correoE + "," + organizacion + "," + usuario + "," + clave + "," + comprador + "," + vendedor;
    } //Se realiza el toString con todos los datos del usuario

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }
        Usuario other = (Usuario) o;
        return this.cedula.equals(other.cedula);//El equals comprobara si dos usuarios son iguales chequeando si sus cedulas son iguales
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 37 * hash + Objects.hashCode(this.cedula);
        hash = 37 * hash + Objects.hashCode(this.correoE);
        hash = 37 * hash + Objects.hashCode(this.usuario);
        return hash;//El hashCode se lo realizo utilizando los atributos del usuario que son unicos como su cedula, correoE y su usuario
    }

}
