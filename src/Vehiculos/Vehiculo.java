package Vehiculos;

import Utilidades.Venta;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;

public class Vehiculo {

    protected String tipo;
    protected String placa;
    protected String marca;
    protected String modelo;
    protected String tipoMotor;
    protected int año;
    protected double recorrido;
    protected String color;
    protected String tipoCombustible;
    protected boolean vidrios;
    protected String transmision;
    protected double precio;
    ArrayList<Venta> listaVentas;

    public Vehiculo(String tipo, String placa, String marca, String modelo, String tipoMotor, int año, double recorrido, String color, String tipoCombustible, Boolean vidrio, String transmision, double precio) {
        this.tipo = tipo;
        this.placa = placa.toUpperCase();
        this.marca = marca.toUpperCase();
        this.modelo = modelo.toUpperCase();
        this.tipoMotor = tipoMotor.toUpperCase();
        this.año = año;
        this.recorrido = recorrido;
        this.color = color.toUpperCase();
        this.tipoCombustible = tipoCombustible.toUpperCase();
        this.vidrios = vidrio;
        this.transmision = transmision.toUpperCase();
        this.precio = precio;
        this.listaVentas = new ArrayList<>();
    }

    public Vehiculo(String placa) {
        this.placa = placa.toUpperCase();
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getTipoMotor() {
        return tipoMotor;
    }

    public void setTipoMotor(String tipoMotor) {
        this.tipoMotor = tipoMotor;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public int getAño() {
        return año;
    }

    public void setAño(int año) {
        this.año = año;
    }

    public double getRecorrido() {
        return recorrido;
    }

    public void setRecorrido(double recorrido) {
        this.recorrido = recorrido;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public boolean isVidrios() {
        return vidrios;
    }

    public void setVidrios(boolean vidrios) {
        this.vidrios = vidrios;
    }

    public String getTransmision() {
        return transmision;
    }

    public void setTransmision(String transmision) {
        this.transmision = transmision;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public ArrayList<Venta> getListaVentas() {
        return listaVentas;
    }

    public void setListaVentas(ArrayList<Venta> listaVentas) {
        this.listaVentas = listaVentas;
    }

    public static ArrayList<Vehiculo> leer_RegistroVechiculos() {
        ArrayList<Vehiculo> vehiculos = new ArrayList<>();//Se define una lista que recibe Vehiculos
        try (Scanner datos = new Scanner(new File("vehiculos.txt"))) {//Se utiliza el Scanner para leer el archivo que llamaremos vehiculos.txt
            while (datos.hasNextLine()) {//Si la variables datos tiene una siguiente linea, se procedera a realizar lo que esta a continuacion
                String[] linea = datos.nextLine().split(",");//Se crea un arreglo de tipo string utilizando el split en la coma de la variable de datos
                String tipo = linea[0];//Se define la variable tipo de tipo String sacando el elemento de linea en indice 0
                String placa = linea[1];//Se define la variable placa de tipo String sacando el elemento de linea en indice 1
                String marca = linea[2];//Se define la variable marca de tipo String sacando el elemento de linea en indice 2
                String modelo = linea[3];//Se define la variable modelo de tipo String sacando el elemento de linea en indice 3
                String tipo_motor = linea[4];//Se define la variable tipo_motor de tipo String sacando el elemento de linea en indice 4
                int año = Integer.parseInt(linea[5]);//Se define la variable año de tipo int sacando el elemento de linea en indice 5 y convertirlo a un entero con el metodo Integer.parseInt()
                double recorrido = Double.parseDouble(linea[6]);//Se define la variable recorrido de tipo double sacando el elemento de linea en indice 6 y convertirlo a un double con el metodo Double.parseDouble()
                String color = linea[7];//Se define la variable color de tipo String sacando el elemento de linea en indice 7
                String tipo_combustible = linea[8];//Se define la variable tipo_combustible de tipo String sacando el elemento de linea en indice 8
                Boolean vidrios = Boolean.parseBoolean(linea[9]);//Se define la variable vidrios de tipo Boolean sacando el elemento de linea en indice 9 y convierte a Boolean con el metodo Boolean.parseBoolean()
                String transmicion = linea[10];//Se define la variable transmicion de tipo String sacando el elemento de linea en indice 10
                double precio = Double.parseDouble(linea[11]);//Se define la variable precio de tipo double sacando el elemento de linea en indice 11 y convertirlo a un double con el metodo Double.parseDouble(
                vehiculos.add(new Vehiculo(tipo, placa, marca, modelo, tipo_motor, año, recorrido, color, tipo_combustible, vidrios,
                        transmicion, precio));//Se agrega el objeto de tipo Vehiculo con todos los elementos definidos anteriormente y se lo agrega a la lista vehiculos
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());//Si no se puede realizar lo que esta dentro del try, se arroja un mensaje avisando que no fue posible realizar el proceso
        }
        return vehiculos;//Se retorna la lista vehiculos
    }

    public void registrarVehiculo() {
        try (FileWriter datos = new FileWriter("vehiculos.txt", true);//Se crea una variable datos de tipo FileWriter para escribir el archivo vehiculos.txt
                BufferedWriter bufer = new BufferedWriter(datos);//Se define una variable llamada bufer de tipo BufferedWriter que escribira el archivo que recibe datos
                PrintWriter agregar = new PrintWriter(bufer)) {//Se procedera a escribir el archivo que recibe bufer mediante la clase PrintWriter
            agregar.println(this);
        } catch (IOException ex) {
        }
    }

    @Override
    public String toString() {
        return tipo + "," + placa + "," + marca + "," + modelo + "," + tipoMotor + "," + año + "," + recorrido + "," + color + "," + tipoCombustible + "," + vidrios + "," + transmision + "," + precio;
    }//Se realiza el toString mostrando todos los atributos que recibe un vehiculo con su respectiva informacion

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        if (o == this) {
            return true;
        }
        if (o.getClass() != this.getClass()) {
            return false;
        }
        Vehiculo other = (Vehiculo) o;
        return (this.placa.equals(other.placa));//Se vera si dos vehiculos son iguales comparando si sus placas son las mismas
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.placa);
        hash = 59 * hash + this.año;
        hash = 59 * hash + (int) (Double.doubleToLongBits(this.recorrido) ^ (Double.doubleToLongBits(this.recorrido) >>> 32));
        hash = 59 * hash + (this.vidrios ? 1 : 0);
        return hash;//Para el hashCode se utilizaran los atributos de placa, año, su recorrido y los vidrios
    }

}
