package Vehiculos;

public class Camioneta extends Vehiculo {

    public Camioneta(String placa, String marca, String modelo, String tipoMotor, int año, double recorrido, String color, String tipoCombustible, String transmision, double precio) {
        super("Camioneta", placa, marca, modelo, tipoMotor, año, recorrido, color, tipoCombustible, true, transmision, precio);

    }//Se realizo el constructor de la clase Camioneta que recibe sus atributos y los de la super clase Vehiculo utilizando el super

}
